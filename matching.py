# coding: utf-8

import numpy as np
import pandas as pd
import pyvista as pv
import open3d as o3d
import glob
import copy

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from scipy.spatial import distance

def get_df(polydata):
    df = pd.DataFrame(polydata.points, columns=["x", "y", "z"])
    return df

def get_poly(df):
    array_3D = np.array(df)
    poly = pv.PolyData(array_3D)
    return poly


def draw_registration_result(source, target, transformation):
    source_temp = copy.deepcopy(source)
    target_temp = copy.deepcopy(target)
    source_temp.paint_uniform_color([1, 0.706, 0])
    target_temp.paint_uniform_color([0, 0.651, 0.929])
    source_temp.transform(transformation)
    o3d.visualization.draw_geometries([source_temp, target_temp])

def icp(source, target, downsample):
    source_pcd = o3d.io.read_point_cloud(source)
    target_pcd = o3d.io.read_point_cloud(target)
    print(f'Center of source_pcd mesh: {source_pcd.get_center()}')
    print(f'Center of target_pcd mesh: {target_pcd.get_center()}')
    

    print("Load a ply point cloud, print it, and render it")
    print(source_pcd)
    print(np.asarray(source_pcd.points))

    #o3d.visualization.draw_geometries([source_pcd])
    print("Load a ply point cloud, print it, and render it")
    print(target_pcd)
    print(np.asarray(target_pcd.points))
    #o3d.visualization.draw_geometries([target_pcd])
    
        # pre-proc ICP
    print("***Remove outliers source")
    source_pcd, ind = source_pcd.remove_statistical_outlier(nb_neighbors=50, std_ratio=0.5) 

    if downsample == True:
        print("Downsample the point cloud with a voxel of 0.05")
        down_source_pcd = source_pcd.voxel_down_sample(voxel_size=0.05)
        print("Downsample the point cloud with a voxel of 0.05")
        down_target_pcd = target_pcd.voxel_down_sample(voxel_size=0.05)
        # o3d.visualization.draw_geometries([down_source_pcd, down_target_pcd])

    else:
        down_source_pcd = source_pcd
        down_target_pcd = target_pcd

    print("Recompute the normal of the downsampled point cloud")
    down_source_pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
    down_target_pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))

    o3d.io.write_point_cloud("down_source.ply", down_source_pcd)
    o3d.io.write_point_cloud("down_target.ply", down_target_pcd)

    print("Apply point-to-point ICP")
    print(type(down_source_pcd))
    print(type(down_target_pcd))
    reg_p2p = o3d.registration.registration_icp(down_source_pcd, down_target_pcd, max_correspondence_distance=1000)
    print(reg_p2p)
    print("Transformation is:")
    print(reg_p2p.transformation)
    draw_registration_result(down_source_pcd, down_target_pcd, reg_p2p.transformation)
    return reg_p2p.transformation

source = pv.PolyData("reco_mesh.ply") # la mesh ricostruita
target = pv.PolyData("target.ply") # il 'cad'

source.translate([-source.center[0],-source.center[1], -source.center[2]])
source.save("mesh_trasl.ply")

target.translate([-target.center[0],-target.center[1], -target.center[2]])
target.save("target_trasl.ply")
print(target.bounds)
x_len = distance.euclidean(target.bounds[0], target.bounds[1])
y_len = distance.euclidean(target.bounds[2], target.bounds[3])
z_len = distance.euclidean(target.bounds[4], target.bounds[5])
print("target dim: ", x_len, y_len, z_len)

mtx_affine = icp("mesh_trasl.ply", "target_trasl.ply", downsample=True)

source.transform(np.array(mtx_affine))
source.save("mesh_transf.ply")

pad = 20

roi = pv.Cube(center=target.center,
              x_length=x_len+pad,
              y_length=y_len+pad,
              z_length=z_len+pad)         
extracted = source.clip_box(roi, invert=False)
extracted_poly = pv.PolyData(extracted.points)
extracted_poly.save("mesh_clip.ply")


mtx_affine_2 = icp("mesh_clip.ply", "target_trasl.ply", downsample=True)
source.transform(np.array(mtx_affine_2))
source.save("mesh_transf_2.ply")

pad = 0.3

roi = pv.Cube(center=target.center,
              x_length=x_len+pad,
              y_length=y_len+pad,
              z_length=z_len+pad)         
extracted = source.clip_box(roi, invert=False)
extracted_poly = pv.PolyData(extracted.points)
extracted_poly.save("mesh_clip_2.ply")








