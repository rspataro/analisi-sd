import json
import subprocess

# process = subprocess.run(["ls", "-l", "/dev/null"], stdout=subprocess.PIPE)
# process.stdout
# process.returncode


meshlab_server = r"/home/MeshLab2020.09-linux/AppRunMeshLabServer"
reco = r"/home/asset_esterni/registrazioneOut.obj"
mlx_filter_strisciantiInterp = r"/home/sistema-diagnostica/analisi-sd/meshlab-filters/filtro1.mlx"
mlx_filter_merge = r"/home/sistema-diagnostica/analisi-sd/meshlab-filters/merge_AKA_flatten.mlx"
# mlx_filter2 = r"/home/sistema-diagnostica/analisi-sd/meshlab-filters/extrStrisciantiReco.mlx"
strisciantiInterp = r"/home/asset_esterni/test/strisciantiInterp.obj"
# out2= r"/home/asset_esterni/test/out.ply"
merge = r"/home/asset_esterni/test/merge.obj"

_file = open(r"/home/sistema-diagnostica/analisi-sd/template/modelA.json", "r")
model = json.load(_file)
_file.close()

print(model["fpathTarget"])
process = subprocess.run([meshlab_server, "-i", reco, "-i", model["fpathTarget"], "-o", strisciantiInterp, "-l", "2", "-s", mlx_filter_strisciantiInterp], stdout=subprocess.PIPE)

if process.returncode == 0:
    pass
else:
    print("errore")

process = subprocess.run([meshlab_server, "-i", strisciantiInterp, "-i", model["fpathTargetNoStrip"], "-o", merge, "-l", "c", "-s", mlx_filter_merge], stdout=subprocess.PIPE)

if process.returncode == 0:
    pass
else:
    print("errore")
# process2 = subprocess.run([meshlab_server, "-i", reco, "-o", out2, "-l", "1", "-s", mlx_filter2], stdout=subprocess.PIPE)

# process = subprocess.run(["ls", "-l", "/dev/null"], stdout=subprocess.PIPE)
# print(process.stdout)
# print(process.returncode)